<?php 

/**
 * Plugin Name: Linkmix
 * Plugin URI:
 * Description: A plugin to create social link landing pages and get analytics from them
 * Version: 1.0
 * Author: Centauri Systems
 * Author URI: http://centauri.website
 * License: GPL2
 */

define( 'LINKMIX_PATH', plugin_dir_path( __FILE__ ) );

/**
 * Add a menu to the Wordpress Admin panel
 */


 add_action( 'admin_menu', 'register_custom_admin_page' ); // Hook to add admin page


 function register_custom_admin_page() { // Add a top-level navigation interface for the Admin control panel
    add_menu_page( __( 'Custom Page Title', 'textdomain' ), 'Linkmix', 'manage_options', 'linkmix', 'custom_menu_page', 'dashicons-chart-area', 110 );
    add_submenu_page('linkmix', 'Submenu Page Title', 'Overview', 'manage_options', 'overview', 'custom_submenu_page' );
    remove_submenu_page('linkmix','linkmix'); // Removes the default admin page from the submenu
 }


 function custom_menu_page(){
    require_once( LINKMIX_PATH . 'template/admin.php');
 }

 function custom_submenu_page() {
    require_once( LINKMIX_PATH . 'template/overview.php');
 }


 add_action('admin_menu', 'enqueue_admin');

 function enqueue_admin() { // Add static files to admin menu
    wp_enqueue_style( 'linkmix-admin-style', plugins_url( LINKMIX_PATH . './static/admin.css' ) );
    wp_register_style( 'font_awesome', 'http:////maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css' );
    wp_enqueue_script( 'linkmix-admin-script', plugins_url( LINKMIX_PATH . './static/admin.js' ) );
 }

 add_action('init', 'create_custom_post_type' ); 

 function create_custom_post_type() { // Add custom post types to admin page

    $labels = array(
        'name'                  => _x( 'Links', 'Post type general name', 'textdomain' ),
        'singular_name'         => _x( 'Link', 'Post type singular name', 'textdomain' ),
        'add_new'               => __( 'Add New' ),
		'add_new_item'          => __( 'Add New Link' ),
		'edit_item'             => __( 'Edit Link' ),
		'new_item'              => __( 'Add New Link' ),
		'view_item'             => __( 'View Link' ),
		'search_items'          => __( 'Search Link' ),
    );

    $supports = array(
		'editor',
		'author',
        'custom-fields',
        'post-formats',
	);

    $args = array(
        'public'             => true,
        'labels'             => $labels,
        'hierarchical'       => true,
        'menu_position'      => 5,
        'menu_icon'          => 'dashicons-image-filter',
        'supports'           => $supports,
        'has_archive'        => true,
    );

    register_post_type( 'link', $args ); 
 }

 add_action('add_meta_boxes', 'link_details_box');

 function link_details_box() {
	add_meta_box(
		'wpt_links_page',
		'Link Page',
		'wpt_links_page',
		'links',
		'normal',
		'default'
	);
}

 // Add activation, deactivation, and uninstall hooks functionally?

 /**register_activation_hook(__FILE__, array($lmp, 'activate'));

// deactivation

register_deactivation_hook(__FILE__, array($lmp, 'deactivate'));

// admin settings

// uninstall

register_uninstall_hook(__FILE__, array($lmp, 'uninstall'));
  */

?>