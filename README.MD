# Linkmix

A link curation plugin for Cartez A.


##  Sitemap

## ./linkmix.php

Holds the plugin, its configuration, subclassing, hooks, etc.

### ./template/..

Holds the direct render pages of the admin menu and submenus. `./includes` are imported into here and `./static` referenced.

### ./includes/..

Holds all of the functional modules for the admin menu pages, like the chart visualization and archive of post types

### ./static/..

Holds all of the static files needed for modules in the admin menu.

### ./public/..

Holds all of the templating files to display the links in the user's custom set link from the admin menu
